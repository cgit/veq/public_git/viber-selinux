# defining macros needed by SELinux
%global selinuxtype targeted
%global selinux_policyver 3.13.1-1
%global moduletype contrib
%global modulename viber
%global debug_package %{nil}

Name:		viber-selinux	
Version:	1.0
Release:	1%{?dist}
Summary:	SELinux policies for viber

License:	License: GPLv3
URL:		git://fedorapeople.org/home/fedora/veq/public_git/viber-selinux.git
Source0:	viber-selinux.tar.gz

BuildArch: noarch

BuildRequires:	git
BuildRequires: pkgconfig(systemd)
BuildRequires: selinux-policy
BuildRequires: selinux-policy-devel
Requires:	selinux-policy >= %{selinux_policyver}
Requires(post): selinux-policy-base >= %{selinux_policyver}
Requires(post): libselinux-utils
Requires(post): policycoreutils
%if 0%{?fedora}
Requires(post): policycoreutils-python-utils
%else
Requires(post): policycoreutils-python
%endif


%description
SELinux policy modules for viber


%prep
%setup -q


%build
make


%pre
%selinux_relabel_pre -s %{selinuxtype}


%install
# install policy modules
install -d %{buildroot}%{_datadir}/selinux/packages
install -d -p %{buildroot}%{_datadir}/selinux/devel/include/%{moduletype}
install -p -m 644 %{modulename}.if %{buildroot}%{_datadir}/selinux/devel/include/%{moduletype}
install -m 0644 %{modulename}.pp.bz2 %{buildroot}%{_datadir}/selinux/packages


%post
%selinux_modules_install -s %{selinuxtype} %{_datadir}/selinux/packages/%{modulename}.pp.bz2


%postun
if [ $1 -eq 0 ]; then
    %selinux_modules_uninstall -s %{selinuxtype} %{modulename}
fi


%posttrans
%selinux_relabel_post -s %{selinuxtype}


%files
%defattr(-,root,root,0755)
%attr(0644,root,root) %{_datadir}/selinux/packages/%{modulename}.pp.bz2
%attr(0644,root,root) %{_datadir}/selinux/devel/include/%{moduletype}/%{modulename}.if


%changelog
* Tue Nov 13 2018 Viktar Siarheichyk <veq@fedoraproject.org> - 0.1.0-1
- First Build
